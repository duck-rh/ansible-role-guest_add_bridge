Ansible role to add a network interface to a guest VM

This role use libvirt and virt-xml to dynamically add a network
interface to a VM running on libvirt. It is made to be used along the
role guest_virt_install, who support only 1 single interface.

# Usage

To use it, just add the role after the guest_virt_install in the playbook.

For example:

```
---
- hosts: hypervisor.example.org
  roles:
  - role: guest_virt_install
    name: test.example.org
    bridge: "{{ bridge_public }}"
    volgroup: "{{ vm_storage_vg  }}"
    system_disk_size: 10
    mem_size: 1024
    distribution: Fedora
    version: 25
    sshkey: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
  - role: guest_add_bridge
    name: test.example.org
    bridge: "{{ bridge_private }}"
```

This will add the bridge from the bridge_private (for example, br2) to the VM, without
reboot or anything. There is a module bundled that can be used to also remove the interface,
and set a different hypervisor, but that's not exposed with the current role.
